#include "headers/BatteryState.h"
#include "headers/DisplayParent.h"
#include "headers/Display.h"
#include "headers/Parent.h"
#include "headers/XbeeParent.h"
#include "headers/XbeeReceive.h"

const int sendPin = 8;
const int ledPin = 9;
int papa = 0;
int mama = 1;

unsigned long starttime, endtime;

/*Initialisierung Parent 0=Papa, 1=Mama*/
Parent parent(mama);
DisplayParent displayParent(parent);
XBeeAddress64 addr64 = XBeeAddress64(0x0013A200, 0x40E621D5);

XBee xbee = XBee();
XbeeParent xbeeparent(xbee);
XbeeReceive xbeeReceive;
uint8_t payload[] = {(uint8_t)mama};

void setup() {
  // put your setup code here, to run once:
  //Serial1.begin(9600);
  Display();
  BatteryState batteryState('o');
 
 Serial.begin(9600);
 xbeeparent.initialAddress(addr64);
 xbeeReceive.initialLed();
 pinMode(sendPin, INPUT);
 pinMode(ledPin, OUTPUT);
}

bool printParentTextState = false; 
void loop() 
{
      xbeeReceive.readPacket();
      if(digitalRead(sendPin) == 1)
      {
        sendMessage();
      }
      
      if(xbeeReceive.getData == true)
      {
        
         displayParent.printParentText();
         starttime = millis();
         endtime = starttime + 30000;
         printParentTextState = true;
      } else if( millis() > endtime)
      {
        if(printParentTextState)
        {
          displayParent.clearDisplay();
          BatteryState batteryState('o');
          printParentTextState = false;
        }
      }
      
      
     /*if (Serial1.available()){
        Serial.println("hier");
        displayParent.clearDisplay();
     } else {
        //Serial.println("");
     }*/
  
}

void sendMessage() {
   xbeeparent.XbeeSend(payload);
  
}

