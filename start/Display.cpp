#include "headers/Display.h"

Adafruit_SSD1306 display(OLED_RESET);

Display::Display()
{
	Serial.begin(9600);
	display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
	display.clearDisplay();
}