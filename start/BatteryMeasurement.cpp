#include "headers/BatteryMeasurement.h"

/*Source from 
 * https://provideyourown.com/2012/secret-arduino-voltmeter-measure-battery-voltage/
*/

BatteryMeasurement::BatteryMeasurement()
{
  #if defined(__AVR_ATmega2560__)
    ADMUX = _BV(REFS0) | _BV(MUX4) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  #else
    ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  #endif 
}

long BatteryMeasurement::readVcc()
{
	delay(2); // Wait for Vref to settle
	ADCSRA |= _BV(ADSC); // Start conversion
	while (bit_is_set(ADCSRA,ADSC)); // measuring

  	uint8_t low  = ADCL; // must read ADCL first - it then locks ADCH  
  	uint8_t high = ADCH; // unlocks both

  	long result = (high<<8) | low;

  	result = 1125300L / result; // Calculate Vcc (in mV); 1125300 = 1.1*1023*1000
  	return result; // Vcc in millivolts
}