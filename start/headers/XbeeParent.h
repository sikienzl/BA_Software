#ifndef XBEEPARENT_H
#define XBEEPARENT_H

#include "Arduino.h"
#include "XBee.h"

extern XBee xbee;

class XbeeParent
{

	public:
		XbeeParent(XBee xbeeUeberg);
		void initialAddress(XBeeAddress64 addr64);
		void XbeeSend(uint8_t payload[]);

	private:
		XBeeAddress64 addr64;
		ZBTxRequest zbTx;
		ZBTxStatusResponse txStatus;

};

#endif