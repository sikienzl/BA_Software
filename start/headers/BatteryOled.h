#ifndef BATTERYOLED_H
#define BATTERYOLED_H

#include "Arduino.h"
#include "Display.h"

#define LOGO16_GLCD_HEIGHT 16 
#define LOGO16_GLCD_WIDTH  16 

static const unsigned char PROGMEM logo16_glcd_bmp[] = { B00111111, B11111100,
		  B00100000, B00000100,
		  B11100000, B00000100,
		  B11100000, B00000100,
		  B11100000, B00000100,
		  B00100000, B00000100,
		  B00111111, B11111100,
		  B00000000, B00000000};	

class BatteryOled
{

	public:
		BatteryOled(void);
		void printBatteryState(char state);
	private:
		void printLowBlock(uint16_t state);
		//Adafruit_SSD1306 displayOled;
};



 
#endif