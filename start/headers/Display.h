#ifndef DISPLAY_H
#define DISPLAY_H

#include "Arduino.h"
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define OLED_RESET 4

#define NUMFLAKES 10
#define XPOS 0
#define YPOS 1
#define DELTAY 2

extern Adafruit_SSD1306 display;

class Display
{
	public:
		Display();
		
		
};

#endif