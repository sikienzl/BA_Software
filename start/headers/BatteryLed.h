#ifndef BATTERYLED_H
#define BATTERYLED_H

#define BLUE_PIN 10
#define RED_PIN 9
#define GREEN_PIN 8

#include "Arduino.h"

class BatteryLed
{
	public:
		BatteryLed(void);
		void light(char color);
	private:
		int redState;
		int greenState;
		int blueState;
		void lightToggle(void);
		void lightOff(void);
};

#endif
