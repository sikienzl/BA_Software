#ifndef BATTERYMEASUREMENT_H
#define BATTERYMEASUREMENT_H

#include "Arduino.h"

class BatteryMeasurement
{
	public:
		BatteryMeasurement(void);
		long readVcc(void);
};

#endif