#ifndef DISPLAYPARENT_H
#define DISPLAYPARENT_H

#include "Arduino.h"
#include "Display.h"
#include "Parent.h"



class DisplayParent
{

	public:
		DisplayParent(Parent);
		void clearDisplay(void);
		void printParentText(void);
	private:
		String parentString;
		Parent parent;
		void setParent(void);
		

};

#endif