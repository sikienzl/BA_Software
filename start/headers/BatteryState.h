#ifndef BATTERYSTATE_H
#define BATTERYSTATE_H

#include "Arduino.h"
#include "BatteryMeasurement.h"
#include "BatteryLed.h"
#include "BatteryOled.h"

/*max-value here: 4979*/

#define FULL_MIN_VALUE 3735
#define MIDDLE_MIN_VALUE 2491
#define LOW_MIN_VALUE 1247

class BatteryState
{
	public:
		BatteryState(char type);		
	private:
		BatteryMeasurement batteryMeasurement;
		BatteryLed batteryLed;
		BatteryOled batteryOled;
};

#endif