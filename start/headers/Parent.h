#ifndef PARENT_H
#define PARENT_H


class Parent
{
	private:
		
		void setParent(int);
		
	public:
		int varParent = 0;
		Parent(int);
		Parent(void);
};

#endif