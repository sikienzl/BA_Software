#ifndef XBEERECEIVE_H
#define XBEERECEIVE_H

#include "Arduino.h"
#include "XBee.h"
#include "XbeeParent.h"

class XbeeReceive
{
	public:
		XbeeReceive(void);
		int getState(void);
		void readPacket(void);
		void initialLed(void);
		bool getData = false;
	private:
		void setState(int value);
		void flashLed(int pin, int times, int wait);
		int state;
		XBeeResponse response;
		ZBRxResponse rx;
		ModemStatusResponse msr;
		int statusLed = 13;
		int errorLed = 13;
		int dataLed = 13;
};

#endif