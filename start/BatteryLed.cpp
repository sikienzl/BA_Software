#include "headers/BatteryLed.h"

int redState;
int greenState;
int blueState;

BatteryLed::BatteryLed(void)
{
	pinMode(BLUE_PIN, OUTPUT);
	pinMode(RED_PIN, OUTPUT);
	pinMode(GREEN_PIN, OUTPUT);
}

void BatteryLed::light(char color)
{
	redState = 0;
  greenState = 0;
  blueState = 0;
	if(color == 'r')
	{
		redState = 255;
		greenState = 255;
		blueState = 0;
	}
	else if(color == 'y')
	{
		redState = 150;
		greenState = 255;
		blueState = 0;
	}
	else if(color == 'g')
	{
		redState = 0;
		greenState = 255;
		blueState = 200;
	}
	else if(color == 't')
	{
      lightToggle();
   }
   analogWrite(RED_PIN, redState);
	 analogWrite(GREEN_PIN, greenState);
   analogWrite(BLUE_PIN, blueState);
}

void BatteryLed::lightToggle()
{
  while(1)
  {
   redState = 255;
   greenState = 255;
   blueState = 0;
   analogWrite(RED_PIN, redState);
   analogWrite(GREEN_PIN, greenState);
   analogWrite(BLUE_PIN, blueState);
   delay(1000);
   lightOff();
   delay(1000);
  }
}

void BatteryLed::lightOff()
{
      redState = 255;
      greenState = 255;
      blueState = 255;
      analogWrite(RED_PIN, redState);
      analogWrite(GREEN_PIN, greenState);
      analogWrite(BLUE_PIN, blueState);
}

