#include "headers/XbeeParent.h"

ZBTxRequest zbTx;
ZBTxStatusResponse txStatus;
XbeeParent::XbeeParent(XBee xbeeUeberg)
{
	xbee = xbeeUeberg;
	Serial1.begin(9600);
	xbee.setSerial(Serial1);
}

void XbeeParent::initialAddress(XBeeAddress64 addresse)
{
	
	addr64 = addresse;
}
void XbeeParent::XbeeSend(uint8_t payload[])
{
	txStatus = ZBTxStatusResponse();
	zbTx = ZBTxRequest(addr64, payload, sizeof(payload));
	xbee.send(zbTx);
	if(xbee.readPacket(500))
	{
		if (xbee.getResponse().getApiId() == ZB_TX_STATUS_RESPONSE) 
		{
      		xbee.getResponse().getZBTxStatusResponse(txStatus);
      		
    		if (txStatus.getDeliveryStatus() == SUCCESS) {
		       
      		} else {
      			Serial.println("NO SUCCESS");
     		}
    	}
  	} else if (xbee.getResponse().isError()) {
    } else {
    }
}