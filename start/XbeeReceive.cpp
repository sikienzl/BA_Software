#include "headers/XbeeReceive.h"


XBeeResponse response = XBeeResponse();
// create reusable response objects for responses we expect to handle 
ZBRxResponse rx = ZBRxResponse();
ModemStatusResponse msr = ModemStatusResponse();

XbeeReceive::XbeeReceive()
{
	//xbee = xbeeUeberg;
	
}
void XbeeReceive::initialLed()
{
	pinMode(statusLed, OUTPUT);
  pinMode(errorLed, OUTPUT);
  pinMode(dataLed,  OUTPUT);
}
void XbeeReceive::flashLed(int pin, int times, int wait) {
    
    for (int i = 0; i < times; i++) {
      digitalWrite(pin, HIGH);
      delay(wait);
      digitalWrite(pin, LOW);
      
      if (i + 1 < times) {
        delay(wait);
      }
    }
}
void XbeeReceive::readPacket()
{
	
	xbee.readPacket();
    
    if (xbee.getResponse().isAvailable()) {
      if (xbee.getResponse().getApiId() == ZB_RX_RESPONSE) {
        xbee.getResponse().getZBRxResponse(rx);
        
        if (rx.getOption() == ZB_PACKET_ACKNOWLEDGED) {
            /*for (int i = 0; i < rx.getDataLength(); i++) { 
                Serial.println(rx.getData(i), HEX);
            }*/
            getData = true;
            if(rx.getDataLength() == 1)
            {
            	setState(rx.getData(0));
            }
        } else {
        	            flashLed(errorLed, 2, 20);

        }
      } else if (xbee.getResponse().getApiId() == MODEM_STATUS_RESPONSE) {
        xbee.getResponse().getModemStatusResponse(msr);
        
        if (msr.getStatus() == ASSOCIATED) {
        	          flashLed(statusLed, 10, 10);

        } else if (msr.getStatus() == DISASSOCIATED) {
        	          flashLed(errorLed, 10, 10);

        } else {
        	flashLed(statusLed, 5, 10);

        }
      } else {
      	flashLed(errorLed, 1, 25);    
      }
    } else if (xbee.getResponse().isError()) {
    } else {
    	getData = false;
    }
}

void XbeeReceive::setState(int value)
{
	if(value == 48)
		state = 0;
	if(value == 49)
		state = 1;
}

int XbeeReceive::getState()
{
	return state;
}

