#include "headers/BatteryOled.h"

BatteryOled::BatteryOled()
{
	/*Prints the structure of a battery*/
	display.drawBitmap(0, 0,  logo16_glcd_bmp, 16, 8, 1);
  	display.display();
  	delay(1);
}

void BatteryOled::printBatteryState(char state)
{

	uint16_t state_color_full = WHITE;
	uint16_t state_color_middle = WHITE;
	uint16_t state_color_low = WHITE;
	
	bool critical = false;

	/*  f = full
	 *  m = middle
	 *  l = low
	 *  c = critical 
	*/
	if (state == 'f')
	{
		
	}
	else if(state == 'm')
	{
		state_color_full = BLACK;
	}
	else if(state == 'l')
	{
		state_color_full = BLACK;
		state_color_middle = BLACK;
	}
	else if(state == 'c')
	{
		state_color_full = BLACK;
		state_color_middle = BLACK;
		critical = true;
	}


	/*
	*
	*	first block of battery
	*/
  display.drawPixel(4, 2, state_color_full);
  display.drawPixel(4, 3, state_color_full);
  display.drawPixel(4, 4, state_color_full);
  display.drawPixel(5, 2, state_color_full);  
  display.drawPixel(5, 3, state_color_full);
  display.drawPixel(5, 4, state_color_full);

  /*
	*
	*	middle block of battery
  */

  display.drawPixel(7, 2, state_color_middle);
  display.drawPixel(7, 3, state_color_middle);
  display.drawPixel(7, 4, state_color_middle);
  display.drawPixel(8, 2, state_color_middle);  
  display.drawPixel(8, 3, state_color_middle);
  display.drawPixel(8, 4, state_color_middle);
  
  printLowBlock(state_color_low);
  

  while(critical)
	{
			state_color_low = WHITE;
			printLowBlock(state_color_low);
			display.display();
  			delay(1000);
  			state_color_low = BLACK;
			printLowBlock(state_color_low);
  			display.display();
  			delay(1000);
	}

  

  display.display();
	
}

/*
	*
	*	last block of battery
  */

void BatteryOled::printLowBlock(uint16_t state)
{
	display.drawPixel(10, 2, state);  
  	display.drawPixel(10, 3, state);
  	display.drawPixel(10, 4, state);
  	display.drawPixel(11, 2, state);  
  	display.drawPixel(11, 3, state);
  	display.drawPixel(11, 4, state);
}
