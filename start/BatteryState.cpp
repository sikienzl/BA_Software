#include "headers/BatteryState.h"

BatteryState::BatteryState(char type)
{
	if(batteryMeasurement.readVcc() > FULL_MIN_VALUE)
	{

		if(type == 'o')
		{
			batteryOled.printBatteryState('f');
		} else {
			batteryLed.light('g');	
		}
		
	} else if (batteryMeasurement.readVcc() > MIDDLE_MIN_VALUE || 
				batteryMeasurement.readVcc() < FULL_MIN_VALUE)
	{
		if(type == 'o')
		{
			batteryOled.printBatteryState('m');
		} else {
			batteryLed.light('y');	
		}
		
	} else if (batteryMeasurement.readVcc() > LOW_MIN_VALUE ||
			   batteryMeasurement.readVcc() < MIDDLE_MIN_VALUE)
	{
		if(type == 'o')
		{
			batteryOled.printBatteryState('l');
		} else {
			batteryLed.light('r');
		}
	} else if (batteryMeasurement.readVcc() < LOW_MIN_VALUE) {
    	if(type == 'o')
		{
			batteryOled.printBatteryState('c');
		} else {
    		batteryLed.light('t');
		}
	}
}
